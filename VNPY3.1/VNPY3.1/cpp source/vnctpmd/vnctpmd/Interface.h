/*
1.本文件为VNTrader 期货CTP交易库底层代码
2.VNTrader及本C++库开源协议GPLV3协议

对VNPY开源库做出贡献的，并得到原始作者肯定的，将公布在http://www.vnpy.cn网站上，
并添加在《开源说明和感谢.txt》，并将该文件不断更新放入每一个新版本的vnpy库里。

官方网站：http://www.vnpy.cn
*/
#pragma once

#ifdef PYCTPMARKET_EXPORTS
#define VN_EXPORT __declspec(dllexport)
#else
#define VN_EXPORT __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"{
#endif 

	struct VNInstrument
	{
		TThostFtdcInstrumentIDType InstrumentID;
	};

	void VN_EXPORT VNRegOnFrontConnected(void(*outputCallback)());

	void VN_EXPORT VNRegOnFrontDisconnected(void(*outputCallback)(int a));

	void VN_EXPORT VNRegOnRspUserLogin(void(*outputCallback)(const CThostFtdcRspUserLoginField * a));

	void VN_EXPORT VNRegOnRspUserLogout(void(*outputCallback)(const CThostFtdcUserLogoutField * a));

	void VN_EXPORT VNRegOnRtnDepthMarketData(void(*outputCallback)(const CThostFtdcDepthMarketDataField* a));

	void VN_EXPORT VNRegOnRspUnSubMarketData(void(*outputCallback)(const TThostFtdcInstrumentIDType * a));

	void VN_EXPORT VNRegOnRspSubMarketData(void(*outputCallback)(const TThostFtdcInstrumentIDType * a));

	int VN_EXPORT InitMD();

	VN_EXPORT char *  GetApiVersion();

	VN_EXPORT char *  GetTradingDay();

	void VN_EXPORT RegisterFront(char *pszFrontAddress);

	void VN_EXPORT RegisterNameServer(char *pszNsAddress);

	void VN_EXPORT *GetKlineData(int i, int j);

	int VN_EXPORT GetKlineMaxLen();

	int VN_EXPORT GetInstrumentNum();

	void VN_EXPORT *GetKline(VNInstrument  *InstrumentID, int j);

	void VN_EXPORT *GetKline2(VNInstrument  *InstrumentID, int j);

	void VN_EXPORT OpenLog();

	void VN_EXPORT CloseLog();
	
	//用户登录请求
	int VN_EXPORT ReqUserLogin();

	//用户登出请求
	int VN_EXPORT ReqUserLogout();

	//订阅行情
	int VN_EXPORT SubscribeMarketData2(VNInstrument  *InstrumentID);

	//订阅行情
	int VN_EXPORT SubscribeMarketData(VNInstrument  *InstrumentID);

	//取消订阅行情
	int VN_EXPORT UnSubscribeMarketData(VNInstrument *InstrumentID);

    //询价
	void VN_EXPORT SubscribeForQuoteRsp(char *InstrumentID);

	void VN_EXPORT SetPrintState(bool printfstate);

	void VN_EXPORT SetRejectdataTime(double  begintime1,double endtime1,double begintime2,double endtime2, double begintime3, double endtime3, double begintime4, double endtime4);

	void VN_EXPORT TestArr(char** pIpAddList);

	void VN_EXPORT Log(const char * filename, const char * content);
 
	int VN_EXPORT IsInitOK();

	bool VN_EXPORT CROSSUP(const char *InstrumentID, int indicators,int periodtype, int PriceType, int period1, int period2, bool printstate);

	bool VN_EXPORT CROSSDOWN(const char *InstrumentID, int indicators, int periodtype, int PriceType, int period1, int period2, bool printstate);

	bool VN_EXPORT CROSSUP_S(int strategyid, int periodtype, int PriceType, int period1, int period2);

	bool VN_EXPORT CROSSDOWN_S(int strategyid, int periodtype, int PriceType, int period1, int period2);


#ifdef __cplusplus
}
#endif